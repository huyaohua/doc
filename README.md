
## bitforex系统概览

### bitforex主要系统模块图例

```mermaid
graph LR
    PC --> Gateway[nginx-openresty]
    IOS --> Gateway
    Android --> Gateway
    Other --> Gateway

    Gateway --> bitforex-web[bitforex-web用户钱包以及历史api]
    Gateway --> spot-trade-api[币币交易行情api trade-api]
    Gateway --> per-api[永续api]
    Gateway --> bitforex-oauth[开放平台]
    Gateway --> mt5-api
    Gateway --websocket--> market-system
    bitforex-web --> user-system[用户服务]
    bitforex-web --> asset-system[资产服务]
    bitforex-web --> message-tool[资产服务]
    bitforex-web --> bf-coin[平台币服务]
    per-api---asset-tranfer
    mt5-api---asset-tranfer
    bitforex-oauth ---asset-tranfer
    user-system --> message-tool[邮件短信服务]
    asset-system --> message-tool[邮件短信服务]
    asset-system --- asset-tranfer[资产划转]
    bitforex-web --> support-system[广告其他服务]
    bitforex-web --> market-system[币币行情查询服务]
    spot-trade-api --> market-system
    spot-trade-api --查询活跃订单--> spot-order[币币订单处理系统-多个子系统]
    spot-trade-api --取消单--> MQ
    MQ[币币rocketmq]-->spot-asset-service[币币资产清算系统-多个子服务]
    spot-asset-service-->MQ
    spot-match[币币撮合]-->MQ
    MQ-->spot-match
    MQ--> spot-order
    MQ--> market-handle

    Gateway --websocket/rest--> market-per-system[永续行情查询]

    p-match[永续撮合]-->PMQ
    PMQ[永续rocketmq]-->p-match
    PMQ --> p-clear[永续清算]
    PMQ --> qingpnig[永续强平]
    PMQ --> market-per-handle
    per-api --> PMQ




    subgraph api Layer
     bitforex-web
     spot-trade-api
     per-api
     mt5-api
     bitforex-oauth
    end

    subgraph 基础服务 Layer
     user-system
     asset-system
     message-tool
     support-system
     asset-tranfer
     bf-coin
    end

  

    subgraph 永续系统集群 Layer
    subgraph 永续行情 Layer
    market-per-system
    market-per-handle
    end
    PMQ 
    p-match
    p-clear
    qingpnig
    end


    subgraph 币币交易系统集群 Layer
    subgraph 币币行情 Layer
    market-system
    market-handle
    end
    MQ
    spot-match
    spot-order
    spot-asset-service
    end

    subgraph Access Layer
    subgraph Front
    PC
    IOS
    Android
    end
    Other
    end

```

>系统所有的项目除了Gateway模块外，语言主要均采用java，开发框架为springcloud；服务之间调用采用springcloud feign + eureka;

### 相关模块说明

#### Gateway [nginx-openresty]
这一模块，使用的是openresty框架【nginx+lua】,主要是两个作用
1. 使用nginx作为反向代理，作为根据不同的模块进行url路由选择
2. 使用openresty框架，即nginx+lua进行限流，整个限流的代码工程为_[openresty-limit](https://git.bitforex.com/bitforex-java-new/openresty-limit)_


---

#### 基础系统模块
这一模块，提供基础的用户，短信邮件以及其他周边功能, 所有系统模块包含如下几个:
###### [bitforex-web](https://git.bitforex.com/java/bitforex-web.git)
- 此系统是bitforex的一个非常老的项目，曾经他是所有功能的api入口
- 现在此系统是除了文档中所列出的其他api功能外的所有api接口的入口
- 包含了用户系统，钱包资产系统以及历史各种功能的api综合入口
- 此系统现在有如下问题：
  - 项目代码比较臃肿，历史3年，曾经一切的api功能都会写到此系统中，造成了代码较乱，而且有好多历史无用的功能
  - 历史较久，有一定的性能瓶颈；维护性比较差
  
###### [用户服务](https://git.bitforex.com/java/subsystem_user.git)
- 此系统为用户系统，提供用户认证，安全以及各种基础用户功能；其他项目通过微服务调用此系统

###### [资产钱包服务](https://git.bitforex.com/java/subsystem-assets.git)
- 该模块是公司老项目，提供用户钱包账户和币币账户服务，主要涉及资产查询，流水查询，充值，提现等相关操作。一开始只有钱包账户，后增加币币账户

###### [短信邮件服务](https://git.bitforex.com/java/subsystem-messtool.git)
- 此模块主要是提供短信网关以及邮件网关服务

###### [平台币服务](https://git.bitforex.com/java/subsystem-platformcoin.git)
- 平台币服务，现在只有一个功能，每天定时将用户锁仓的bf释放给用户

###### [功能支撑服务](https://git.bitforex.com/java/subsystem-support.git)
- 此服务主要提供，广告管理以及语言管理服务等功能


---

#### 币币交易系统模块
- 此系统模块主要是币币交易系统，其总体设计文档和模块作用可以查看--[币币交易系统设计文档](https://git.bitforex.com/doc/new-spot-doc/wikis/%E5%B8%81%E5%B8%81%E4%BA%A4%E6%98%93%E7%B3%BB%E7%BB%9F%E6%80%BB%E4%BD%93%E8%AE%BE%E8%AE%A1%E6%96%87%E6%A1%A3)
- 币币交易系统一共分为如下几个模块


###### [spot-trade-api](https://git.bitforex.com/bitforex-trade-new/spot-trade-api)
- 币币交易系统，交易api项目，对外提供交易的rest api；包含交易，行情，币币资产



###### [bitforex-match-v2](https://git.bitforex.com/bitforex-trade-new/bitforex-match-v2)
- 币币交易系统撮合系统
 

###### [币币交易系统资产清算模块](https://git.bitforex.com/bitforex-trade-new/bitforex-trade-assets)
- 资产清算模块，包含一个内置的资产内存数据库以及币币交易的资产清算逻辑，其一共有如下几个模块
- 包含三个模块
   - server交易核心，接受用户命令，更新币币资产，生成订单等
   - slave模块读取最新币币资产，提供给用户查询
   - job oplog落库以及生成资产快照，资产流水
  

###### [币币订单处理模块](https://git.bitforex.com/bitforex-trade-new/bitforex-trade-order)
- 包含了一个单独的活跃订单内存数据存储以及订单持久化存储计算系统

###### [币币行情模块](https://git.bitforex.com/bitforex-java-new/subsystem-market-spot)
- 包含了两个模块
  - market-query，查询模块，提供了行情数据的查询接口
  - maket-handler 行情计算模块

---

#### 永续交易系统
###### [永续行情模块](https://git.bitforex.com/bitforex-java-new/subsystem-market-perpetual)
- 包含了两个模块【注名：和币币行情页面，70%代码重合，稍后会单独说明行业模块，未来可以和币币行情模块合并】
  - market-query，查询模块，提供了行情数据的查询接口
  - maket-handler 行情计算模块
  
###### [永续服务](https://git.bitforex.com/bitforex-java-new/perpetual.git)
- 项目采用maven模块化开发，此工程包含了多个项目模块，同时也囊括了后台管理项目
- 包含了永续API服务、清算服务、强平服务、定时任务、后台管理服务
  - api 接口模块，提供了前端交互请求接口
  - clear 清算模块
  - liquidation 强平模块
  - ppmanagement 后台管理模块
  - xxljob 永续相关定时任务模块

--- 

#### mt5系统
###### [mt5模块](https://git.bitforex.com/bitforex-java-new/mt5.git)
- 包含了mt5-api服务、mt5定时任务服务
  - api 提供页面交互接口服务（同时底层对接MT5服务）
  - mt5-job mt5相关定时任务模块

### dapp
- 开放平台账户管理，资产管理
